/*
 * Copyright (C) 2013, 2014, 2015  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <sys/wait.h>
#include <ctype.h>
#include <errno.h>
#include <gcrypt.h>
#include <inttypes.h>
#include <limits.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <httpserver.h>
#include "capysd.h"
#include "environ.h"

/*
 * Spawn the utility program (this is mostly like CGI code).
 * The external utility program will authenticate, change to
 * user rights and dir and write result, which we got and
 * send to the requester.
 */

#if 0
static void childreap(int sid) {}
#endif

int spawn_request(struct http_connection *conn,
					const char *program,
					const char *user,
					const char *authtok,
					const char *service,
					const char *scope)
{
	struct http_request *rq = http_get_request(conn), ri;
	char *args[2] = {NULL, NULL};
	struct env_blk env;
	pid_t pid;
	int fdin[2], fdout[2], headers_len, data_len, i;
	FILE *in, *out, *fout;
	const char *status, *status_text;
	int status_code;
	char buf[8192], *pbuf, *p;
	const char *connection;
#if 0
	sighandler_t sigchld;
#endif

	http_trace(conn, "spawning %s for user %s", program, user);

	args[0] = (char *)program;

	prepare_env(conn, &env);

	pid = -1;
	fdin[0] = fdin[1] = fdout[0] = fdout[1] = -1;
	in = out = NULL;

#if 0
	sigchld = signal(SIGCHLD, childreap);
#endif

	if (pipe(fdin) != 0 || pipe(fdout) != 0) {
		http_error(conn, "Cannot create pipe: %m");
		http_send_response(conn, 500, "Internal Server Error");
		goto done;
	}

	pid = http_spawn_process(conn, args, env.vars, fdin[0], fdout[1], "/");
	fdin[0] = fdout[1] = -1; /*TODO*/

	if (pid < 0) {
		http_error(conn, "Cannot spawn capys process [%s]: %m", program);
		http_send_response(conn, 500, "Internal Server Error");
		goto done;
	}

	if (!(in = fdopen(fdin[1], "wb")) ||
			!(out = fdopen(fdout[0], "rb"))) {
		http_error(conn, "Cannot open descriptor: %m");
		http_send_response(conn, 500, "Internal Server Error");
		goto done;
	}

	setbuf(in, NULL);
	setbuf(out, NULL);
	fout = out;

	/* Send the user/password or user/token credentials */

	fprintf(in, "%s\n%s\n%s\n%s\n", user, authtok, service, scope);

	/* Send data to the process if needed */
	if (http_get_content_len(conn) > 0 && 
			!http_forward_body_data(conn, in, -1, NULL))
		goto done;

	/* Close so child gets an EOF. */
	fclose(in);
	in = NULL;
	fdin[1] = -1;

	/*
	 * Now read reply into a buffer. We need to set correct
	 * status code, thus we need to see all HTTP headers first.
	 * Do not send anything back to client, until we buffer in all
	 * HTTP headers.
	 */
	data_len = 0;
	headers_len = http_read_request(conn, out, buf, sizeof(buf), &data_len);
	if (headers_len <= 0) {
		http_error(conn, 
				"capys external program (%s) sent malformed or too big (>%u bytes) "
				"HTTP headers: [%.*s]",
				program, (unsigned) sizeof(buf), data_len, buf);
		http_send_response(conn, 500, "Internal Server Error");
		goto done;
	}
	pbuf = buf;
	buf[headers_len - 1] = '\0';
	http_parse_headers(&pbuf, &ri);

	/* Make up and send the status line */
	status_code = 200;
	status_text = "OK";
	if ((status = get_header(&ri, "Status")) != NULL) {
		status_code = atoi(status);
		status_text = status;
		while (isdigit(*status_text) || *status_text == ' ')
			status_text++;
	}
	else if (get_header(&ri, "Location") != NULL)
		status_code = 302;
	else
		status_code = 200;

	http_set_status_code(conn, status_code);
	http_printf(conn, "HTTP/1.1 %d %s\r\n", status_code, status_text);

	if (get_header(&ri, "Server") == NULL)
		http_printf(conn, "Server: %s\r\n", http_get_config(conn)->server_software);
	if (get_header(&ri, "Date") == NULL) {
		char dbuf[64];
		http_printf(conn, "Date: %s\r\n", date(dbuf, sizeof(dbuf), 0));
	}
	if (http_get_header(conn, "Origin"))
		http_printf(conn, "Access-Control-Allow-Origin: *\r\n");
	if (get_header(&ri, "Cache-Control") == NULL && 
			get_header(&ri, "Content-Length") != NULL)
		http_printf(conn, "Cache-Control: no-cache\r\n");

	/*
	 * If Connection: is specified, honor it.
	 * Else close the connection on fatal error
	 */
	if ((connection = get_header(&ri, "Connection")) != NULL) {
		if (strcasecmp(connection, "keep-alive") != 0)
			http_must_close(conn);
	}
	else {
		if (status_code == 401 || status_code == 404 || status_code >= 500) {
			http_must_close(conn);
			http_printf(conn, "Connection: close\r\n");
		}
		else
			http_printf(conn, "Connection: keep-alive\r\n");
	}

	/* Send headers, except the Status: one. */
	for (i = 0; i < ri.nheaders; i++) {
		if (strcasecmp(ri.headers[i].name, "Status") == 0)
			continue;
		http_printf(conn, "%s: %s\r\n",
				ri.headers[i].name, ri.headers[i].value);
	}
	http_write(conn, "\r\n", 2);

	/* Send chunk of data that may have been read after the headers */
	http_add_bytes_sent(conn, http_write(conn, buf + headers_len,
								 (size_t)(data_len - headers_len)));

	/* Read the rest of output and send to the client */
	http_send_file_data(conn, fout, 0, 0, INT64_MAX);

	if (waitpid(pid, NULL, 0) < 0) {
		http_error(conn, "Cannot waitpid(): %m");
		http_send_response(conn, 500, "Internal Server Error");
		goto done;
	}

done:
	if (pid > 0)
		kill(pid, SIGKILL);
#if 0
	signal(SIGCHLD, sigchld);
#endif
	if (fdin[0] != -1)
		close(fdin[0]);
	if (fdout[1] != -1)
		close(fdout[1]);

	if (in != NULL)
		fclose(in);
	else if (fdin[1] != -1)
		close(fdin[1]);

	if (out != NULL)
		fclose(out);
	else if (fdout[0] != -1)
		close(fdout[0]);
	http_trace(conn, "external_handle done");
	return 1;
}

