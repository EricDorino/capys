/*
 * Copyright (C) 2013, 2014, 2015 Éric Dorino
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <httpserver.h>
#include <limits.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "capysd.h"

extern int capys_begin_request(struct http_connection *);
extern int capys_http_response(struct http_connection *,
														 	 int, const char *, va_list);

static int exit_flag = 0;
static struct http_context *ctx;

static void signal_handler(int sig)
{
  exit_flag = sig;
}

#define OPT	256
#define OPT_DOMAIN						(OPT+0)
#define OPT_DOCROOT						(OPT+1)
#define OPT_LOG_FACILITY			(OPT+2)
#define OPT_URL_START					(OPT+3)

static struct option opt[] = {
	{"domain", required_argument, NULL, OPT_DOMAIN},
	{"debug", no_argument, NULL, 'd'},
	{"document-root", required_argument, NULL, OPT_DOCROOT},
	{"enable-keep-alive", no_argument, NULL, 'K'},
	{"foreground", no_argument, NULL, 'F'},
	{"help", no_argument, NULL, 'h'},
	{"local", no_argument, NULL, 'l'},
	{"log-facility", required_argument, NULL, OPT_LOG_FACILITY},
	{"port", required_argument, NULL, 'p'},
	{"ssl-certificate", required_argument, NULL, 'C'},
	{"ssl-port", required_argument, NULL, 'P'},
	{"threads", required_argument, NULL, 'T'},
	{"timeout", required_argument, NULL, 't'},
	{"user", required_argument, NULL, 'u'},
	{"verbose", no_argument, NULL, 'v'},
	{"version", no_argument, NULL, 'V'},
	{NULL, 0, NULL, 0}
};

#define DEFAULT_DOCROOT				"/var/www"
#define DEFAULT_LOG_FACILITY	"local0"
#define DEFAULT_PORT					"80"
#define DEFAULT_SSL_PORT			"443"
#define DEFAULT_NTHREADS			"50"
#define DEFAULT_TIMEOUT				"30000"
#define DEFAULT_USER					"www-data"

static struct {
	const char *comment;
	const char *default_value;
} opt_opt[] = {
	{"authentification domain", NULL},		/* MUST remains at index 0 */
	{"enter debug mode, do not daemonize", NULL},
	{"document root", DEFAULT_DOCROOT},
	{"enable keep alive connections by default", NULL},
	{"stay in foreground, do not daemonize", NULL},
	{"show help and exit", NULL},
	{"accept only connections from localhost", NULL},
	{"syslog facility", DEFAULT_LOG_FACILITY},
	{"HTTP port", DEFAULT_PORT},
	{"SSL PEM certificate", NULL},
	{"secure HTTP port", DEFAULT_SSL_PORT},
	{"number of threads", DEFAULT_NTHREADS},
	{"request timeout (ms)", DEFAULT_TIMEOUT},
	{"user to run daemon", DEFAULT_USER},
	{"be verbose", NULL},
	{"show version and exit", NULL}
};

static void usage(void)
{
	size_t i;

  fputs("usage: capysd [options]*\n", stderr);
  fputs("options:\n", stderr);

  for (i = 0; opt[i].name; i++) {
    fprintf(stderr, "  %c%c%s--%s: %s %c%s%c\n",
										opt[i].val < OPT?'-':'\0', 
										opt[i].val < OPT?opt[i].val:'\0', 
										opt[i].val < OPT?", ":"", 
										opt[i].name, 
										opt_opt[i].comment, 
										opt_opt[i].default_value?'(':'\0',
										opt_opt[i].default_value?opt_opt[i].default_value:"",
										opt_opt[i].default_value?')':'\0');
  }
  exit(EXIT_FAILURE);
}

static void log_message(const struct http_connection *conn,
								int priority, const char *format, va_list args)
{
  vsyslog(priority, format, args);
}

int main(int argc, char **argv) {
	int c;
  struct http_callbacks callbacks;
	struct http_config config;
	int port = atoi(DEFAULT_PORT),
			ssl_port = atoi(DEFAULT_SSL_PORT),
			local = 0,
			facility = LOG_LOCAL0;
	const char *rewrite_rules[] = {};
	char server[255], domain[128], server_software[128];

  memset(&config, 0, sizeof(config));
	config.daemonize = 1;
	if (getdomainname(domain, sizeof(domain)) < 0 || 
			strcmp(domain, "(none)") == 0)
		strcpy(domain, "domain.invalid");
	opt_opt[0].default_value = domain;
	config.domain = domain;
	if (gethostname(server, sizeof(server)) < 0)
		strcpy(server, "server");
	if (!index(server, '.')) {
		strncat(server, ".", sizeof(server));
		strncat(server, domain, sizeof(server));
	}
	config.server = server;
	config.auth_type = "basic";
	config.auth_program = PKGLIBEXECDIR "/capys_auth";
	config.nthreads = atoi(DEFAULT_NTHREADS);
	config.timeout = atoi(DEFAULT_TIMEOUT);
	config.docroot = DEFAULT_DOCROOT;
	config.user = DEFAULT_USER;
	snprintf(server_software, sizeof(server_software),
					"capys/%s libhttp/%s libtls/%s",
					VERSION, http_version(), http_tls_version());
	config.server_software = server_software;
	config.hide_files = "^\\.|^~|.*~$";
	config.index_files = "index.html";
	config.cgi_pattern = "^.*\\.cgi$";
	config.protected_uri = "^/capys/api/.*$";

  memset(&callbacks, 0, sizeof(callbacks));
  callbacks.log_message = log_message;
	callbacks.begin_request = capys_begin_request;
	callbacks.http_response = capys_http_response;

	opterr = 0;
	for (;;) {
		int index = 0;

		if ((c = getopt_long(argc, argv, "C:dFhKlP:p:T:t:u:vV", opt, &index)) < 0)
			break;

		switch (c) {
			default:
				usage();
				return EXIT_FAILURE;
			case 'C':
				config.ssl_certificate = optarg;
				break;
			case 'K':
				config.keep_alive = 1;
				break;
			case 'l':
				local = 1;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case 'P':
				ssl_port = atoi(optarg);
				break;
			case 'T':
				config.nthreads = atoi(optarg);
				break;
			case 't':
				config.timeout = atoi(optarg);
				break;
			case 'u':
				config.user = optarg;
				break;
			case OPT_DOMAIN:
				config.domain = optarg;
				break;
			case OPT_DOCROOT:
				config.docroot = optarg;
				break;
			case OPT_LOG_FACILITY:
					if (strncmp(optarg, "local", 5) == 0 && strlen(optarg) == 6 && 
							optarg[5] >= '0' && optarg[5] <= '7')
						facility = LOG_LOCAL0 + atoi(optarg+5);
					else if (strcmp(optarg, "daemon") == 0)
						facility = LOG_DAEMON;
					else if (strcmp(optarg, "mail") == 0)
						facility = LOG_MAIL;
					else if (strcmp(optarg, "user") == 0)
						facility = LOG_USER;
					else {
						usage();
						return EXIT_FAILURE;
					}
				break;
			case OPT_URL_START:
				break;
			case 'F':
				config.daemonize = 0;
				break;
			case 'v':
				config.verbose = 1;
				break;
			case 'd':
				config.debug = 1;
				config.verbose = 1;
				break;
			case 'h':
				usage();
				return EXIT_FAILURE;
			case 'V':
				fprintf(stderr, "capysd server, part of %s/%s\n\n",
					PACKAGE_NAME, PACKAGE_VERSION);
				fputs("Copyright (C) 2013-2016  Eric Dorino\n"
					"This is free software; see the source "
					"for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY "
					"of FITNESS FOR A PARTICULAR PURPOSE.\n",
					stderr);
				return EXIT_FAILURE;
		}
	}

	openlog("capysd", LOG_PID|LOG_PERROR, facility);
	if (config.debug)
		setlogmask(LOG_UPTO(LOG_DEBUG));
	else if (config.verbose)
		setlogmask(LOG_UPTO(LOG_INFO));
	else 
		setlogmask(LOG_UPTO(LOG_NOTICE));

  signal(SIGTERM, signal_handler);
  signal(SIGINT, signal_handler);

	config.nrewrite_rules = sizeof(rewrite_rules)/sizeof(rewrite_rules[0]);
	config.rewrite_rules = rewrite_rules;

	config.nports = 0;
	if (port > 0) {
		config.ports[config.nports].port = port;
		config.ports[config.nports].local = local;
		config.ports[config.nports].ipv6 = 0; /* FIXME */
		config.ports[config.nports].is_ssl = 0;
		config.ports[config.nports++].ssl_redir = ssl_port > 0;
	}
	if (ssl_port > 0) {
		config.ports[config.nports].port = ssl_port;
		config.ports[config.nports].local = local;
		config.ports[config.nports].ipv6 = 0; /*FIXME */
		config.ports[config.nports].is_ssl = 1;
		config.ports[config.nports++].ssl_redir = 0;
	}

	if (config.nports == 0) {
    syslog(LOG_ERR, "no valid HTTP/HTTPS ports specified");
		exit(EXIT_FAILURE);
	}

  if (!(ctx = http_start(&callbacks, NULL, &config))) {
    syslog(LOG_ERR, "http_start() failed");
		exit(EXIT_FAILURE);
  }

  syslog(LOG_NOTICE, "capysd server started");

  while (exit_flag == 0)
    sleep(1);

  http_stop(ctx);

  syslog(LOG_NOTICE, "capysd server exited");

  return EXIT_SUCCESS;
}
