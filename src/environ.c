/*
 * Copyright (C) 2013, 2014  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "environ.h"

static char *add_envvar(struct env_blk *blk, const char *fmt, ...)
{
	int remains, n;
	char *var;
	va_list params;

	remains = sizeof(blk->buf) - blk->len - 2;

	var = blk->buf + blk->len;

	va_start(params, fmt);
	n = vsnprintf(var, remains, fmt, params);
	va_end(params);

	if (n > 0 && n +1 < remains && blk->nvars < ARRAY_SIZE(blk->vars) -2) {
		blk->vars[blk->nvars++] = var;
		blk->len += n + 1;
		return var;
	}
	return NULL;
}

void prepare_env(struct http_connection *conn, struct env_blk *blk)
{
	int i;
	struct http_request *rq = http_get_request(conn);
	struct http_config *cf = http_get_config(conn);

	blk->len = blk->nvars = 0;

	add_envvar(blk, "REQUEST_METHOD=%s", rq->method);
	add_envvar(blk, "REQUEST_URI=%s", rq->uri);
	if (rq->query_string)
		add_envvar(blk, "QUERY_STRING=%s", rq->query_string);
	if (rq->remote_user)
		add_envvar(blk, "REMOTE_USER=%s", rq->remote_user);

	for (i = 0; i < rq->nheaders; i++) {
		char *p = add_envvar(blk, "HTTP_%s=%s",
			rq->headers[i].name,
			rq->headers[i].value);
		for (; *p != '\0' && *p != '='; p++)
			if (*p == '-') *p = '_';
			else *p = toupper(*p);
	}
	add_envvar(blk, "X_DEBUG=%d", cf->debug);
	add_envvar(blk, "X_VERBOSE=%d", cf->verbose);
	add_envvar(blk, "X_DOMAIN=%s", cf->domain);

	blk->vars[blk->nvars++] = NULL;
	blk->buf[blk->len++] = '\0';
}
