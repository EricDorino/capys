/*
 * Copyright (C) 2013, 2014  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef environ_h_
#define environ_h_
#include <httpserver.h>

#define ENV_SIZE				2048
#define MAX_ENV_VARS		32
#define ARRAY_SIZE(a)		(sizeof(a)/sizeof(a[0]))

struct env_blk {
	char buf[ENV_SIZE];
	int len;
	char *vars[MAX_ENV_VARS];
	size_t nvars;
};

extern void prepare_env(struct http_connection *, struct env_blk *);

#endif
