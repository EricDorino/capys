/*
 * Copyright (C) 2015, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <sys/types.h>
#include <dirent.h>
#include <dlfcn.h>
#include <limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

struct credentials {
	const char *user, *authtok;
};

static int conv(int nmsgs, const struct pam_message **msgs,
								struct pam_response **resp, void *data)
{
	int i;

	if (!msgs || !resp || !data)
		return PAM_CONV_ERR;

	if (!(*resp = malloc(nmsgs * sizeof(struct pam_response))))
		return PAM_CONV_ERR;

	for (i = 0; i < nmsgs; i++) {
		resp[i]->resp_retcode = 0;
		resp[i]->resp = NULL;
		switch (msgs[i]->msg_style) {
			case PAM_PROMPT_ECHO_ON:
				resp[i]->resp = strdup(((struct credentials *)data)->user);
				break;
			case PAM_PROMPT_ECHO_OFF:
				resp[i]->resp = strdup(((struct credentials *)data)->authtok);
				break;
			case PAM_TEXT_INFO:
			case PAM_ERROR_MSG:
				break;
			default:
				free(*resp);
				return PAM_CONV_ERR;
		}
	}
	return PAM_SUCCESS;
}

int debug, verbose;

#define BUFSZ	256

static int change_user(pam_handle_t *pamh, char *user, size_t size)
{
	const char *newuser = NULL;
	int ret;

	ret = pam_get_user(pamh, &newuser, NULL);
	if (ret == PAM_SUCCESS && strcmp(user, newuser) != 0) {
		syslog(LOG_WARNING, "user changed from %s to %s", user, newuser);
		strncpy(user, newuser, size);
	}
	return ret;
}

static int is_valid_scope(const char *scope, const char *appl)
{
	char *s, *p;
	int ret = 0;

	if (!appl || !scope || strlen(scope) == 0)
		return 1;

  if (!(s = strdup(scope)))
		return 0;

	while ((p = strsep(&s, " ")) != NULL)
		if (strcmp(p, appl) == 0)
			ret = 1;

	free(s);
	return ret;
}

int main(void)
{
	char user[BUFSZ], authtok[BUFSZ], service[BUFSZ], scope[BUFSZ], *p;
	struct credentials credentials = {user, authtok};
	struct pam_conv pam_conv = {&conv, &credentials};
	struct passwd *passwd;
	pam_handle_t *pamh = NULL;
	int ret;
	char *method = getenv("REQUEST_METHOD"),
			 *uri = getenv("REQUEST_URI"),
			 *domain = getenv("X_DOMAIN");
	char libname[PATH_MAX], *err, appl[128];
	void *lib;
	void (*fun)(const char *, const char *, char *, char *);

	openlog("capys_appl", isatty(STDERR_FILENO) ? LOG_PERROR : 0, LOG_LOCAL0);

	if (!method)
		method = "GET";
	if (!uri)
		uri = "/capys/api/";

	debug = getenv("X_DEBUG") ? atoi(getenv("X_DEBUG")) : 0;
	verbose = getenv("X_VERBOSE") ? atoi(getenv("X_VERBOSE")) : 0;

	if(debug)
		setlogmask(LOG_UPTO(LOG_DEBUG));
	else if(verbose)
		setlogmask(LOG_UPTO(LOG_INFO));
	else
		setlogmask(LOG_UPTO(LOG_WARNING));

	if (!fgets(user, sizeof(user), stdin) ||
			!fgets(authtok, sizeof(authtok), stdin) ||
			!fgets(service, sizeof(service), stdin) ||
			!fgets(scope, sizeof(scope), stdin)) {
		puts("Status: 500 Internal Server Error\n");
		return EXIT_SUCCESS;
	}
	if ((p = strrchr(user, '\n')))
		*p = '\0';
	if ((p = strrchr(authtok, '\n')))
		*p = '\0';
	if ((p = strrchr(service, '\n')))
		*p = '\0';
	if ((p = strrchr(scope, '\n')))
		*p = '\0';

	ret = pam_start(service, user, &pam_conv, &pamh);
	if (ret == PAM_SUCCESS)
		ret = pam_authenticate(pamh, PAM_SILENT);

	change_user(pamh, user, sizeof(user));

	if (ret == PAM_SUCCESS)
		ret = pam_acct_mgmt(pamh, PAM_SILENT);

	change_user(pamh, user, sizeof(user));

	if (ret == PAM_SUCCESS)
		ret = pam_setcred(pamh, PAM_ESTABLISH_CRED|PAM_SILENT);
#if 0
	if (ret == PAM_SUCCESS)
		ret = pam_open_session(pamh, PAM_SILENT);
#endif

	if (ret != PAM_SUCCESS) {
		printf("Status: 401 Unauthorized\n"
					 "WWW-Authenticate: Basic realm=\"%s\"\n"
					 "WWW-Authenticate: Bearer realm=\"%s\"\n\n",
					 domain, domain);
		syslog(LOG_CRIT, "Auth failed for user %s: %s",
						user, pam_strerror(pamh, ret));
		return EXIT_SUCCESS;
	}
	if (!(passwd = getpwnam(user)) || passwd->pw_uid < 1000) {
		puts("Status: 500 Internal Server Error\n");
		return EXIT_SUCCESS;
	}
	if (setuid(passwd->pw_uid) < 0) {
		syslog(LOG_ERR, "Cannot setuid(): %m");
		puts("Status: 500 Internal Server Error\n");
		return EXIT_SUCCESS;
	}
#if 0
	if (setgid(passwd->pw_gid) < 0) {
		syslog(LOG_ERR, "Cannot setgid(): %m");
		puts("Status: 500 Internal Server Error\n");
		return EXIT_SUCCESS;
	}
#endif
	if (chdir(passwd->pw_dir) < 0) {
		syslog(LOG_ERR, "Cannot chdir(): %m");
		puts("Status: 500 Internal Server Error\n");
		return EXIT_SUCCESS;
	}

	syslog(LOG_DEBUG, "handling %s %s for user %s", method, uri, passwd->pw_name);

	uri += 11;	/* Skip "/capys/api/" */
	if (!(p = index(uri, '/')))
		strcpy(appl, uri);
	else
		strncpy(appl, uri, p-uri);
	uri += strlen(appl);

	if (!is_valid_scope(scope, appl)) {
		puts("Status: 403 Forbidden\n");
		syslog(LOG_ERR, "%s not in scope [%s]", appl, scope);
		return EXIT_SUCCESS;
	}
	snprintf(libname, PATH_MAX - 1 , PKGLIBDIR"/capys_%s.so", appl);
	if (!(lib = dlopen(libname, RTLD_LAZY))) {
		puts("Status: 404 Not Found\n");
		syslog(LOG_ERR, "cannot load capys_%s.so: %s", appl, dlerror());
		return EXIT_SUCCESS;
	}
	if (!(fun = dlsym(lib, appl))) {
		puts("Status: 500 Internal Server Error\n");
		return EXIT_SUCCESS;
	}
	fun(passwd->pw_name, method, uri, getenv("QUERY_STRING"));
	dlclose(lib);

	syslog(LOG_DEBUG, "done %s %s for user %s", method, uri, passwd->pw_name);
#if 0
	pam_close_session(pamh, PAM_SILENT);
#endif
	pam_setcred(pamh, PAM_DELETE_CRED|PAM_SILENT);
	pam_end(pamh, ret);
	closelog();
	return EXIT_SUCCESS;
}
