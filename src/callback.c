/*
 * Copyright (C) 2013, 2014, 2015  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <httpserver.h>
#include <oauth2d.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "capysd.h"

/*
 * Spawn separate process which will:
 * - authenticate
 * - change to user home directory and rights
 * - handle the COR preflight request (eventually).
 * - handle the request.
 */

static int handle_preflight_request(struct http_connection *conn)
{
	const char *method = http_get_header(conn, "Access-Control-Request-Method"),
		*headers = http_get_header(conn, "Access-Control-Request-Headers");
	char dbuf[64];

	if (method && headers &&  
			(strcmp(method, "GET") == 0 ||
			 strcmp(method, "POST") == 0 ||
			 strcmp(method, "PUT") == 0 ||
			 strcmp(method, "DELETE") == 0) &&
			strstr(headers, "authorization")) {
		http_printf(conn, "HTTP/1.1 200 OK\r\n"
				"Server: %s\r\n"
				"Date: %s\r\n"
				"Access-Control-Allow-Origin: *\r\n"
				"Access-Control-Allow-Method: %s\r\n"
				"Access-Control-Allow-Headers: Authorization\r\n\r\n",
			http_get_config(conn)->server_software,
			date(dbuf, sizeof(dbuf), 0),
			method);
	}
	else { /* Deny */
		http_printf(conn, "HTTP/1.1 200 OK\r\n"
				"Server: %s\r\n"
				"Date: %s\r\n\r\n",
			http_get_config(conn)->server_software,
			date(dbuf, sizeof(dbuf), 0));
	}
	return 1;
}

static int handle_request(struct http_connection *conn, const char *program)
{
	struct http_request *rq = http_get_request(conn);
	char dbuf[64];
	const char *password, *auth, *bearer, *user;

	if (strcmp(rq->method, "OPTIONS") == 0 &&
			http_get_header(conn, "Origin")) {
		return handle_preflight_request(conn);
	}

	if ((auth = http_get_header(conn, "Authorization")) &&
					 (bearer = strstr(auth, "Bearer ")) != NULL &&
					 (bearer+7)) {
		struct oa2_token t;
	
		if (oa2_token_find(bearer+7, &t))
			return spawn_request(conn, program,
													 t.user_id, bearer+7, "capys_bearer",
													 t.scope ? t.scope: "");
	}
	else if ((password = http_basic_auth_password(conn)) != NULL) {
		int ret;

		ret = spawn_request(conn, program,
												rq->remote_user, password, "capys_basic", "");
		memset ((char *)password, 0, strlen(password));
		free((void *)password);
		return ret;
	}
	http_send_response(conn, 401, "Unauthorized\r\n"
			"WWW-Authenticate: Basic realm=\"%s\"\r\n"
			"WWW-Authenticate: Bearer realm=\"%s\"",
			http_get_config(conn)->domain,
			http_get_config(conn)->domain);
	return 1;
}

int capys_begin_request(struct http_connection *conn)
{
	struct http_request *rq = http_get_request(conn);

	http_trace(conn, "begin_request %s %s", rq->method, rq->uri);

	if (strncmp(rq->uri, "/capys/api/", 11) == 0)
		return handle_request(conn, PKGLIBEXECDIR "/capys_appl");
	else
		return 0; /* Let the server handle that request */
}

int capys_http_response(struct http_connection *conn, 
						int status, const char *status_text, va_list args)
{
	int close_connection = (status == 401 || status == 404 || status >= 500);
	char dbuf[32];

	http_trace(conn, "http_response %d %s", status, status_text);

	http_set_status_code(conn, status);

	http_printf(conn, "HTTP/1.1 %d ", status);
 	http_vprintf(conn, status_text, args);
	http_printf(conn, "\r\nServer: %s\r\nDate: %s\r\nConnection: %s\r\n\r\n",
		http_get_config(conn)->server_software,
		date(dbuf, sizeof(dbuf), 0),
		close_connection ? "close" : "keep-alive");	
	if (close_connection)
		http_must_close(conn);
	return 1;
}
