/*
 * Copyright (C) 2013, 2014, 2015 Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef capysd_h_
#define capysd_h_
#include <httpserver.h>

extern int spawn_request(struct http_connection *,
								const char *, const char *, const char *, 
								const char *, const char *);

extern char *date(char *, size_t, int);
extern const char *get_header(const struct http_request *, const char *);

#endif
