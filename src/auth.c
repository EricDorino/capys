/*
 * Copyright (C) 2013, 2014,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <security/pam_appl.h>
#include <pwd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

struct userpass {
	const char *user, *pass;
};

static int conv(int nmsgs, const struct pam_message **msgs,
								struct pam_response **resp, void *data)
{
	int i;

	if (!msgs || !resp || !data)
		return PAM_CONV_ERR;

	if (!(*resp = malloc(nmsgs * sizeof(struct pam_response))))
		return PAM_CONV_ERR;

	for (i = 0; i < nmsgs; i++) {
		resp[i]->resp_retcode = 0;
		resp[i]->resp = NULL;
		switch (msgs[i]->msg_style) {
			case PAM_PROMPT_ECHO_ON:
				resp[i]->resp = strdup(((struct userpass *)data)->user);
				break;
			case PAM_PROMPT_ECHO_OFF:
				resp[i]->resp = strdup(((struct userpass *)data)->pass);
				break;
			case PAM_TEXT_INFO:
			case PAM_ERROR_MSG:
				break;
			default:
				free(*resp);
				return PAM_CONV_ERR;
		}
	}
	return PAM_SUCCESS;
}

#define BUFSZ	256

int main(void)
{
	char user[BUFSZ], pass[BUFSZ];
	struct userpass userpass = {user, pass};
	struct pam_conv pam_conv = {&conv, &userpass};
	struct passwd *passwd;
	char *p;
	pam_handle_t *handle = NULL;
	int status;

	openlog("capys_auth", 0, LOG_LOCAL0);

	if (!fgets(user, sizeof(user), stdin) ||
			!fgets(pass, sizeof(pass), stdin)) {
		syslog(LOG_ERR, "No args given");
		return EXIT_FAILURE;
	}
	if ((p = strrchr(user, '\n')))
		*p = '\0';
	if ((p = strrchr(pass, '\n')))
		*p = '\0';

	if (!(passwd = getpwnam(user)) || passwd->pw_uid < 1000)
		return EXIT_FAILURE;

	status = pam_start("capys", user, &pam_conv, &handle);
	if (status == PAM_SUCCESS)
		status = pam_authenticate(handle, PAM_SILENT);
	if (status == PAM_SUCCESS)
		status = pam_acct_mgmt(handle, PAM_SILENT);
	if (status == PAM_SUCCESS)
		status = pam_setcred(handle, PAM_ESTABLISH_CRED|PAM_SILENT);
	pam_end(handle, status);

	if (status == PAM_SUCCESS)
		syslog(LOG_NOTICE, "user %s authentified", user);
	else
		syslog(LOG_WARNING, "authentication error for user %s", user);

	closelog();

	return status == PAM_SUCCESS ? EXIT_SUCCESS : EXIT_FAILURE;
}
