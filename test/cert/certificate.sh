#!/bin/sh

# Clef privée Host
certtool --generate-privkey --outfile privkey.pem
# Clef et Certificat CA
certtool --generate-privkey --outfile ca.privkey.pem
certtool --generate-self-signed --load-privkey ca.privkey.pem --outfile ca.cert.pem
# Certificat Host
certtool --generate-certificate --load-privkey privkey.pem --outfile cert.pem --load-ca-certificate ca.cert.pem --load-ca-privkey ca.privkey.pem

cat cert.pem privkey.pem > server.pem
